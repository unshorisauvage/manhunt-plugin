# Changelog
-----

## Version 1.0.1

### Gameplay adjustements
- Decreased saturation value given to players at the start of the game and on respawn.
    ~~25~~ => *15*

### Mechanics adjusments
- Dead runners are no longer forced to spectate their teammates. (Waiting for a real proper implementation)
- If a headstart delay is specified, hunters can't move during this period. They can still look around.
- Runners now have a certain amount of lives. They cannot be respawed if their lives counter reaches zero.
- Players are healed when they join the server.

### Fixes
- Fixed a bug causing the compass to no cycle through the players.
- Fixed a bug causing the whole plugin to not reckongnize participating players properly when they disconnect.
> This broke the UI, compasses, and respawn system.

-----
