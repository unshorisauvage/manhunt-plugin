# Manhunt 

## Description

Manhunt are gamemodes in minecraft which consists in two teams fighting each other to achieve their goal.
Speedrunners need to kill the Ender Dragon, while Hunters try to kill them.


## Flow

At the start if the game, Hunters are immobilized for 20 seconds. They are given a compass that allows them to track Speedrunners.
The game ends when the dragon is killed, or if all Speedrunners are dead simultaneously.

Fallen Speedrunners can be revived by using a custom item. The item is destroyed after using it.
This is the recipe.
|Crafting  |Table     |Interface  |
|:--------:|:--------:|:---------:|
|GOLD_INGOT|DIAMOND   |GOLD_INGOT |
|GOLD_INGOT|BED       |GOLD_INGOT |
|DIAMOND   |GOLD_INGOT|DIAMOND    |

Hunters can track Speedrunners using a compass: Left-Click changes the tracked player, while Right-Click refreshes its location.

Both teams have their own private chat. They can communicate with each other by tapping `/a message` in chat.

## Commands

- `/a message` - Sends a message in global chat.
- `/hunteradd` - Adds you as a hunter.
- `/runneradd` - Adds you as a runner.
- `/mlist`     - Shows the list of players and the team they're in.
- `/mstart`    - Starts the game.
- `/spectate`  - Adds you as a spectator.


## Config

The config isn't that necessary at the moment, but you want to fill `world_name`.

> This section will be updated when the config evolves.


------

## Developpers

### Prerequisite 

### Maven

You'll need to install [Maven](https://maven.apache.org/index.html) if you don't have it already.
If not you can install it by following [this tutorial](https://maven.apache.org/install.html).

### Compiling

First, download the code, or clone the repo using:
`git clone https://gitlab.com/unshorisauvage/manhunt-plugin.git`

In the `manhunt-plugin` folder, simply type:
- `cd manhunt`
- `mvn install`

You will see a `target` folder, which contains the plugin.

