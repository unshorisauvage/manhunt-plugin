package com.unshorisauvage.util;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.CompassMeta;

public class LodestoneCompassUtil {

    public static ItemStack getLodestoneCompass() {
        ItemStack compass = new ItemStack(Material.COMPASS);
        CompassMeta compassMeta = (CompassMeta) compass.getItemMeta();
        // compassMeta.setLodestone(new Location(Bukkit.getWorld("world"), 0, 0, 0));
        compassMeta.setLodestoneTracked(false);
        compass.setItemMeta(compassMeta);
        return compass;
    }

    public static void setLodestoneLocation(ItemStack compass, Location newLocation) {
        if (compass.getType().equals(Material.COMPASS)) {
            CompassMeta compassMeta = (CompassMeta) compass.getItemMeta();
            compassMeta.setLodestoneTracked(false);
            compassMeta.setLodestone(newLocation);
            compass.setItemMeta(compassMeta);
        }
    }
    
}
