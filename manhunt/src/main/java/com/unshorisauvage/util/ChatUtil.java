package com.unshorisauvage.util;

import com.unshorisauvage.players.ManhuntPlayerV2;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ChatUtil {
    
    public static String buildMessageString(ManhuntPlayerV2 mPlayer, String[] words, ChatChannel chan) {
        Player player = mPlayer.getMCPlayer();
        StringBuilder messageForAllPlayers = new StringBuilder();
        messageForAllPlayers.append("[");
        switch (chan) {
            case ALL:
                messageForAllPlayers.append(ChatColor.GOLD).append("@");
                break;
            case HUNTER:
                messageForAllPlayers.append(ChatColor.RED).append("H");
                break;
            case RUNNER:
                messageForAllPlayers.append(ChatColor.GREEN).append("R");
                break;
            default:
                break;
        }

        messageForAllPlayers.append(ChatColor.WHITE).append("] <");

        switch(mPlayer.getTeamType()) {
            case HUNTER:
                messageForAllPlayers.append(ChatColor.RED).append(player.getName());
                break;
            case RUNNER:
            messageForAllPlayers.append(ChatColor.GREEN).append(player.getName());
                break;
            default:
                break;
        }
        messageForAllPlayers.append(ChatColor.WHITE + "> ");
        for (String str : words) {
            messageForAllPlayers.append(" " + str);
        }
        return messageForAllPlayers.toString();
    }

    public static String buildMessageString(ManhuntPlayerV2 mPlayer, String message, ChatChannel chan) {
        return buildMessageString(mPlayer, message.split(" "), chan);
    }
}
