package com.unshorisauvage.util;

public enum ChatChannel {
    ALL("@"),
    RUNNER("R"),
    HUNTER("H"),
    SPECTATOR("S");

    private ChatChannel(String chan) { }
}
