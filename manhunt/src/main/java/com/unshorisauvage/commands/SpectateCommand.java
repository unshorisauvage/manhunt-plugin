package com.unshorisauvage.commands;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.game.GameState;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SpectateCommand implements CommandExecutor{

    private final GameManager gameManager;

    public SpectateCommand(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
        if (sender instanceof Player) {
            if (gameManager.getGameState() == GameState.WAITING) {
                this.gameManager.getPlayerList().setTeamForPlayer((Player) sender, ManhuntTeamsTypesEnum.SPECTATOR);
                sender.sendMessage("Vous êtes un spectateur.");
            }
        }
        else {
            return false;
        }
        return true;
    }
    
}
