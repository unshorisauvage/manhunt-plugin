package com.unshorisauvage.commands;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.game.GameState;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ManhuntStartCommand implements CommandExecutor{

    private final GameManager gameManager;

    public ManhuntStartCommand(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
        if (sender instanceof Player) {
            if (gameManager.getGameState() == GameState.WAITING) {
                this.gameManager.startGame();
            }
        }
        else {
            return false;
        }
        return true;
    }
    
}
