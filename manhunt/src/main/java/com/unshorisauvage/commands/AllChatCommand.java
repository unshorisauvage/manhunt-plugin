package com.unshorisauvage.commands;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.game.GameState;
import com.unshorisauvage.players.ManhuntPlayerV2;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;
import com.unshorisauvage.util.ChatChannel;
import com.unshorisauvage.util.ChatUtil;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AllChatCommand implements CommandExecutor{

    private final GameManager gameManager;

    public AllChatCommand(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
        if (sender instanceof Player) {
            if (this.gameManager.getGameState() == GameState.GOING) {
                Player player = (Player) sender;
                ManhuntPlayerV2 mPlayer = this.gameManager.getPlayerList().getManhuntPlayerByUUID(player.getUniqueId());
                if (mPlayer.getTeamType() == ManhuntTeamsTypesEnum.SPECTATOR) {
                    player.sendMessage("Les spectateurs ne peuvent parler dans le chat global une fois la partie en cours.");
                    return false;
                }
                String messageForAllPlayers = ChatUtil.buildMessageString(mPlayer, args, ChatChannel.ALL);
                Bukkit.getServer().broadcastMessage(messageForAllPlayers);
                return true;
            }
            else {
                sender.sendMessage("La partie doit avoir démarré pour utiliser cette commande.");
            }
        }
        return true;
    }
}
