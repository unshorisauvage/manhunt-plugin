package com.unshorisauvage.commands;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.util.LodestoneCompassUtil;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DebugCommand implements CommandExecutor{

    private final GameManager gameManager;

    public DebugCommand(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
        if (sender instanceof Player) {
            Player actualPlayer = (Player) sender;
            actualPlayer.getInventory().addItem(LodestoneCompassUtil.getLodestoneCompass());
        }
        return true;
    }
    
}
