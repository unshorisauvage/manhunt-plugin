package com.unshorisauvage.commands;

import com.unshorisauvage.game.GameManager;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class ListCommand implements CommandExecutor{

    private final GameManager gameManager;

    public ListCommand(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] arg3) {
        Bukkit.broadcastMessage(this.gameManager.getPlayerList().getDebug());
        return true;
    }
    
}
