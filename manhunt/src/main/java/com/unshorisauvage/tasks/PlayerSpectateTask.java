package com.unshorisauvage.tasks;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.players.ManhuntPlayerV2;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class PlayerSpectateTask implements Runnable{

    private final GameManager gameManager;
    private final ManhuntPlayerV2 mPlayer;
    private final Entity entityToSpectate;

    public PlayerSpectateTask (GameManager gm, Player player, Player entityToSpectate) {
        this.gameManager = gm;
        this.mPlayer = gm.getPlayerList().getManhuntPlayerByUUID(player.getUniqueId());
        this.entityToSpectate = entityToSpectate;
    }

    @Override
    public void run() {
        mPlayer.getMCPlayer().setSpectatorTarget(this.entityToSpectate);
    }
    
}
