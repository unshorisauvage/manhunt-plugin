package com.unshorisauvage.tasks;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.game.GameState;
import com.unshorisauvage.players.ManhuntPlayerV2;

public class HuntersReleasedTask implements Runnable{

    private GameManager gameManager;

    public HuntersReleasedTask(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @Override
    public void run() {
        switch (this.gameManager.getGameState()) {
            case HEADSTART: 
                for (ManhuntPlayerV2 mPlayer : this.gameManager.getPlayerList().getPlayerList()) {
                    mPlayer.getMCPlayer().sendTitle("Les Chasseurs", "ont étés relachés!", 10, 60, 10);
                }
                this.gameManager.setGameState(GameState.GOING);
                break;
            default:
                break;
        }
    }
}
