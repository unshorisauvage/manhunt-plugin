package com.unshorisauvage.players;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.UUID;

import org.bukkit.entity.Player;

public class ManhuntPlayerList {

    private final ArrayList<ManhuntPlayerV2> playerList;

    public ManhuntPlayerList() {
        this.playerList = new ArrayList<>();
    }

    public void registerPlayer(Player player) {
        if (!this.doesPlayerExistsInList(player)) {
            player.setFoodLevel(20);
            player.setHealth(20D);
            ManhuntPlayerV2 mp = new ManhuntPlayerV2(player, ManhuntTeamsTypesEnum.SPECTATOR);
            this.playerList.add(mp);
            this.playerList.sort(Comparator.comparing(ManhuntPlayerV2::getName));
        }
    }

    public void setTeamForPlayer(Player player, ManhuntTeamsTypesEnum team) {
        ManhuntPlayerV2 mPlayer = this.getManhuntPlayerByUUID(player.getUniqueId());
        if (mPlayer != null) {
            mPlayer.setTeamType(team);
        }
    }

    public ArrayList<ManhuntPlayerV2> getPlayerListByType(ManhuntTeamsTypesEnum type) {
        ArrayList<ManhuntPlayerV2> list = new ArrayList<>();
        for (ManhuntPlayerV2 mPlayerV2 : this.playerList) {
            if (mPlayerV2.getTeamType() == type) {
                list.add(mPlayerV2);
            }
        }
        return list;
    }


    public ArrayList<ManhuntPlayerV2> getPlayerList() {
        return playerList;
    }

    public String getDebug() {
        String res = "Hunters: ";

        ArrayList<ManhuntPlayerV2> list = this.getPlayerListByType(ManhuntTeamsTypesEnum.HUNTER);
        for (ManhuntPlayerV2 mPlayerV2 : list) {
            res = res + mPlayerV2.getName() + " ";
        }

        res = res + "| Runners: ";
        list = this.getPlayerListByType(ManhuntTeamsTypesEnum.RUNNER);
        for (ManhuntPlayerV2 mPlayerV2 : list) {
            res = res + mPlayerV2.getName() + " ";
        }

        res = res + "| Specs: ";
        list = this.getPlayerListByType(ManhuntTeamsTypesEnum.SPECTATOR);
        for (ManhuntPlayerV2 mPlayerV2 : list) {
            res = res + mPlayerV2.getName() + " ";
        }

        return res;
    }

    public void disconnectPlayer(Player p) {
        int index;
        for (index = 0; index < this.playerList.size(); index++) {
            ManhuntPlayerV2 playerV2 = this.playerList.get(index);
            if (playerV2.getUuid().equals(p.getUniqueId())) {
                this.playerList.remove(index);
                break;
            }
        }
    }

    /* Useful methods */
    public ManhuntPlayerV2 getManhuntPlayerByUUID(UUID id) {
        ManhuntPlayerV2 requestedPlayer = null;
        for (ManhuntPlayerV2 mPlayer : this.playerList) {
            if (mPlayer.getUuid().equals(id)) {
                requestedPlayer = mPlayer;
            }
        }
        return requestedPlayer;
    }

    public ManhuntPlayerV2 getManhuntPlayerByName(String playerName) {
        ManhuntPlayerV2 requestedPlayer = null;
        for (ManhuntPlayerV2 mPlayer : this.playerList) {
            if (mPlayer.getName().equalsIgnoreCase(playerName) ) {
                requestedPlayer = mPlayer;
                break;
            }
        }
        return requestedPlayer;
    }

    public boolean doesPlayerExistsInList(Player player) {
        boolean doesPlayerExistsInList = false;
        for (ManhuntPlayerV2 mPlayer : this.playerList) {
            if (mPlayer.getName().equalsIgnoreCase(player.getName())) {
                doesPlayerExistsInList = true;
                break;
            }
        }
        return doesPlayerExistsInList;
    }
}
