package com.unshorisauvage.players;

import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class ManhuntPlayerV2 {

    private UUID uuid;
    private final String name;
    private Player player;
    
    private boolean isAlive;
    private int livesRemaing;
    
    private ManhuntTeamsTypesEnum teamType;

    private int trackedPlayerIndex;

    public ManhuntPlayerV2(Player player, ManhuntTeamsTypesEnum type) {
        this.player = player;
        this.name = player.getName();
        this.uuid = player.getUniqueId();
        this.teamType = type;
        this.isAlive = true;
        this.trackedPlayerIndex = 0;
    }


    /* GETTERS AND SETTERS */
    public UUID getUuid(){
        return this.uuid;
    }

    public String getName() {
        return this.name;
    }

    public Player getMCPlayer() {
        return this.player;
    }

    public void setPlayer(Player player) {
        this.player = player;
        this.uuid = player.getUniqueId();
    }

    public void setRemaingLives(int newAmount) {
        this.livesRemaing = newAmount;
    }
 
    public void setTeamType(ManhuntTeamsTypesEnum newType) {
        this.teamType = newType;
    }
    
    public ManhuntTeamsTypesEnum getTeamType() {
        return this.teamType;
    }

    public void setAlive(boolean isAlive) {
        this.isAlive = isAlive;
    }

    public boolean isAlive() {
        return this.isAlive;
    }

    public boolean isDead() {
        return !this.isAlive;
    }

    public boolean isNotOnline() {
        return !this.player.isOnline();
    }

    public int getTrackedPlayerIndex() {
        return this.trackedPlayerIndex;
    }

    /* ********************** */

    public boolean cantBeRespawned() {
        return this.livesRemaing <= 0 || this.isAlive || !this.player.isOnline();
    }

    public void onPlayerDeath() {
        this.livesRemaing--;
        this.isAlive = false;
    }

    public void reviveAtLocation(Location location) {
        this.isAlive = true;
        Player p = this.getMCPlayer();
        p.setHealth(20D);
        p.setSaturation(25F);
        p.teleport(location);
        p.setGameMode(GameMode.SURVIVAL);
        Bukkit.getLogger().log(Level.INFO, "REVIVE | isAlive=" + this.isAlive + " | Lives:" + this.livesRemaing);
    }

    public int getRemaingLives() {
        return this.livesRemaing;
    }


    public void setTrackedPlayerIndex(int newIndex) {
        this.trackedPlayerIndex = newIndex;
    }


    public ItemStack getHead() {
        ItemStack head = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta headMeta = (SkullMeta) head.getItemMeta();
        headMeta.setOwningPlayer(this.player);
        head.setItemMeta(headMeta);
        return head;
    }

}
