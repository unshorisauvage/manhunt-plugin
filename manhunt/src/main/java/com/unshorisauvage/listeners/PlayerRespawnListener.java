package com.unshorisauvage.listeners;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.players.ManhuntPlayerV2;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;
import com.unshorisauvage.util.LodestoneCompassUtil;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PlayerRespawnListener implements Listener {

    private final GameManager gameManager;

    public PlayerRespawnListener(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @EventHandler(priority=EventPriority.HIGH)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
        switch (this.gameManager.getGameState()) {
            case GOING:
                Player player = event.getPlayer();
                ManhuntPlayerV2 mPlayer = this.gameManager.getPlayerList().getManhuntPlayerByUUID(player.getUniqueId()); 
                if (mPlayer.getTeamType() == ManhuntTeamsTypesEnum.HUNTER) {
                    mPlayer.getMCPlayer().getInventory().addItem(LodestoneCompassUtil.getLodestoneCompass());
                } else if (mPlayer.getTeamType() == ManhuntTeamsTypesEnum.RUNNER) {
                    mPlayer.getMCPlayer().setGameMode(GameMode.SPECTATOR);
                }
                break;
            default:
                break;
        }
        return;
    }
    
}
