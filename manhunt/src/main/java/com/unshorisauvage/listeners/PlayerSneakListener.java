package com.unshorisauvage.listeners;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.game.GameState;
import com.unshorisauvage.players.ManhuntPlayerV2;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;

import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

public class PlayerSneakListener implements Listener {

    private GameManager gameManager;

    public PlayerSneakListener(GameManager gm) {
        this.gameManager = gm;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerSneak(PlayerToggleSneakEvent event) {
        if (this.gameManager.getGameState() == GameState.GOING) {
            ManhuntPlayerV2 mPlayer = this.gameManager.getPlayerList().getManhuntPlayerByUUID(event.getPlayer().getUniqueId());
            if (mPlayer.getTeamType() == ManhuntTeamsTypesEnum.RUNNER && mPlayer.getMCPlayer().getGameMode() == GameMode.SPECTATOR) {
                event.setCancelled(true);
            }
        }
        return;
    }

}