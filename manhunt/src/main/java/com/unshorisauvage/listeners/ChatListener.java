package com.unshorisauvage.listeners;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.game.GameState;
import com.unshorisauvage.players.ManhuntPlayerV2;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;
import com.unshorisauvage.util.ChatChannel;
import com.unshorisauvage.util.ChatUtil;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {

    private final GameManager gameManager;

    public ChatListener(GameManager gm) {
        this.gameManager = gm;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onChatEvent(AsyncPlayerChatEvent event) {
        if (this.gameManager.getGameState() == GameState.GOING) {
            event.setCancelled(true);
            Player player = event.getPlayer();
            ManhuntPlayerV2 mPlayer = this.gameManager.getPlayerList().getManhuntPlayerByUUID(player.getUniqueId());
            String message = null;

            switch (mPlayer.getTeamType()) {
                case HUNTER:
                    message = ChatUtil.buildMessageString(mPlayer, event.getMessage(), ChatChannel.HUNTER);
                    break;
                case RUNNER:
                    message = ChatUtil.buildMessageString(mPlayer, event.getMessage(), ChatChannel.RUNNER);
                    break;
                case SPECTATOR:
                    message = ChatUtil.buildMessageString(mPlayer, event.getMessage(), ChatChannel.SPECTATOR);
                    break;
                default:
                    break;
            
            }
            
            if (message != null) {
                if (!(mPlayer.getTeamType() == ManhuntTeamsTypesEnum.SPECTATOR)) {
                    for (ManhuntPlayerV2 spectator : this.gameManager.getPlayerList().getPlayerListByType(ManhuntTeamsTypesEnum.SPECTATOR)) {
                        spectator.getMCPlayer().sendMessage(message);
                    }
                }
                for (ManhuntPlayerV2 teammate : this.gameManager.getPlayerList().getPlayerListByType(mPlayer.getTeamType())) {
                    teammate.getMCPlayer().sendMessage(message);
                }
            }
        }
    }
}
