package com.unshorisauvage.listeners;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.players.ManhuntPlayerV2;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.inventory.ItemStack;

public class PlayerDamageListener implements Listener{

    private GameManager gameManager;

    public PlayerDamageListener(GameManager gm) {
        this.gameManager = gm;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onDamage(EntityDamageEvent event) {
        if (event.getEntityType() == EntityType.PLAYER) {
            switch (this.gameManager.getGameState()) {
                case WAITING:
                case LOADING:
                    event.setCancelled(true);
                case GOING:
                    break;
                default:
                    break;
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onFoodTick(FoodLevelChangeEvent event) {
        switch (this.gameManager.getGameState()) {
            case WAITING:
            case LOADING:
                event.setCancelled(true);
                break;
            default:
                break;
        }
    }
    
}
