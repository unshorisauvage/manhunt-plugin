package com.unshorisauvage.listeners;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.game.GameState;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockBreakListener implements Listener{

    private final GameManager gameManager;

    public BlockBreakListener(GameManager gm) {
        this.gameManager = gm;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockBroken(BlockBreakEvent event) {
        if (this.gameManager.getGameState() == GameState.WAITING) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockBurnt(BlockBurnEvent event) {
        if (this.gameManager.getGameState() == GameState.WAITING) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onBlockPlaced(BlockPlaceEvent event) {
        if (this.gameManager.getGameState() == GameState.WAITING) {
            event.setCancelled(true);
        }
    }
    
}
