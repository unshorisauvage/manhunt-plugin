package com.unshorisauvage.listeners;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.game.GameState;
import com.unshorisauvage.players.ManhuntPlayerV2;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerMoveListener implements Listener {

    private final GameManager gameManager;

    public PlayerMoveListener(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerMove(PlayerMoveEvent event){
        GameState state = this.gameManager.getGameState();
        switch (state) {
            case LOADING:
            case HEADSTART:
                ManhuntPlayerV2 mPlayer = this.gameManager.getPlayerList().getManhuntPlayerByName(event.getPlayer().getName());
                if (mPlayer.getTeamType() == ManhuntTeamsTypesEnum.HUNTER) {
                    Location fromLoc = event.getFrom();
                    Location toLoc = event.getTo();
                    if (fromLoc.getX() != toLoc.getX() ||
                        fromLoc.getY() != toLoc.getY() ||
                        fromLoc.getZ() != toLoc.getZ()) {
                            event.setCancelled(true);
                        }
                }
                break;
            default:
                break;
        }
    }
    
}
