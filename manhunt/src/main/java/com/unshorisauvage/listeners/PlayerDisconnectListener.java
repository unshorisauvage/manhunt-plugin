package com.unshorisauvage.listeners;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.game.GameState;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerDisconnectListener implements Listener {

    private final GameManager gameManager;

    public PlayerDisconnectListener(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerQuit(PlayerQuitEvent event){
        GameState state = this.gameManager.getGameState();
        switch (state) {
            case WAITING:
            case LOADING:
                this.gameManager.getPlayerList().disconnectPlayer(event.getPlayer());
                break;
            default:
                break;
        }
    }
    
}
