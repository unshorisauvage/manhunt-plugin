package com.unshorisauvage.listeners;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.game.GameState;
import com.unshorisauvage.players.ManhuntPlayerList;
import com.unshorisauvage.players.ManhuntPlayerV2;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

public class PlayerConnectionListener implements Listener {

    // private final PlayerDeathHandler
    private final GameManager gameManager;

    public PlayerConnectionListener(GameManager gameManager) {
        this.gameManager = gameManager;
    }

    @EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerLogin(PlayerLoginEvent event){
        GameState state = this.gameManager.getGameState();
        switch (state) {
            case WAITING:
                this.gameManager.getPlayerList().registerPlayer(event.getPlayer());
                break;
            case LOADING:
                event.setKickMessage("La partie est en chargement. Réessayez dans quelques instants.");
                event.setResult(Result.KICK_OTHER);
                break;
            case GOING:
                break;
            default:
                break;
        }
    }

    @EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent event){
        GameState state = this.gameManager.getGameState();
        Player player = event.getPlayer();
        switch (state) {
            case WAITING:
                this.gameManager.getPlayerList().registerPlayer(player);
                break;
            case LOADING:
                break;
            case GOING:
                ManhuntPlayerList list = this.gameManager.getPlayerList();
                if (list.doesPlayerExistsInList(player)) {
                    ManhuntPlayerV2 mPlayer = list.getManhuntPlayerByName(player.getName());
                    mPlayer.setPlayer(player);
                } else {
                    this.gameManager.getPlayerList().registerPlayer(player);
                    player.setGameMode(GameMode.SPECTATOR);
                }
                break;
            default:
                break;
        }
    }
    
}
