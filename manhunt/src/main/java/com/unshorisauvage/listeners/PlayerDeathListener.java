package com.unshorisauvage.listeners;

import java.util.logging.Level;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.players.ManhuntPlayerV2;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerDeathListener implements Listener {
    
    private GameManager gameManager;

    public PlayerDeathListener(GameManager gm) {
        this.gameManager = gm;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerDeath(PlayerDeathEvent event) {
        ManhuntPlayerV2 mPlayer = this.gameManager.getPlayerList().getManhuntPlayerByUUID(event.getEntity().getUniqueId());
        switch (this.gameManager.getGameState()) {
            case WAITING:
            case LOADING:
                break;
            case HEADSTART:
            case GOING:
                if (mPlayer.getTeamType() == ManhuntTeamsTypesEnum.RUNNER) { 
                    Bukkit.getLogger().log(Level.INFO, "Before on death | isAlive=" + mPlayer.isAlive() + " | Lives:" + mPlayer.getRemaingLives());
                    mPlayer.onPlayerDeath();
                    Bukkit.getLogger().log(Level.INFO, "After on death  | isAlive=" + mPlayer.isAlive() + " | Lives:" + mPlayer.getRemaingLives());
                    Player player = mPlayer.getMCPlayer();
                    player.getWorld().dropItemNaturally(player.getLocation(), mPlayer.getHead());
                    this.gameManager.checkIfAliveRunners();
                } else if (mPlayer.getTeamType() == ManhuntTeamsTypesEnum.HUNTER) {
                    Player player = mPlayer.getMCPlayer();
                    player.getWorld().dropItemNaturally(player.getLocation(), mPlayer.getHead());
                }
            break;
        }
    }
}
