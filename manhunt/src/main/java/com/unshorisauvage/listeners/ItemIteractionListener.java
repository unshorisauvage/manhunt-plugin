package com.unshorisauvage.listeners;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.handlers.ItemInteractionHandler;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

public class ItemIteractionListener implements Listener {

    private ItemInteractionHandler itemInteractionHandler;

    public ItemIteractionListener(GameManager gm) {
        this.itemInteractionHandler = new ItemInteractionHandler(gm);
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onItemInteraction(final PlayerInteractEvent event) {
        this.itemInteractionHandler.handleItemInteraction(event);
    }

}
