package com.unshorisauvage.listeners;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.players.ManhuntPlayerV2;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

public class PlayerTeleportListener implements Listener {

    private GameManager gm;


    public PlayerTeleportListener(GameManager gameManager) {
        this.gm = gameManager;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        switch (this.gm.getGameState()) {
            case GOING:
                ManhuntPlayerV2 mPlayer = this.gm.getPlayerList().getManhuntPlayerByUUID(event.getPlayer().getUniqueId());
                if (event.getCause() == TeleportCause.END_PORTAL){
                    if (mPlayer.getTeamType() == ManhuntTeamsTypesEnum.RUNNER && mPlayer.isAlive()) {
                        StringBuilder message = new StringBuilder();
                        message.append(ChatColor.GREEN + mPlayer.getName() + ChatColor.WHITE +" est entré dans l'End!");
                        Location portalLocation = event.getFrom();
                        String coords = String.format(ChatColor.BLUE + "%.0f" + ChatColor.WHITE + "/" + ChatColor.RED + "%.0f", portalLocation.getX(), portalLocation.getZ());
                        message.append("\nLe portail se trouve en: " + coords);
                        Bukkit.broadcastMessage(message.toString());
                    }
                }
                break;
            default:
                break;
        }
        return;
    }
    
}
