package com.unshorisauvage.listeners;

import java.util.logging.Level;

import com.unshorisauvage.ManhuntMain;
import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.game.GameState;
import com.unshorisauvage.players.ManhuntPlayerV2;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerAdvancementDoneEvent;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TranslatableComponent;
import net.md_5.bungee.api.chat.hover.content.Text;

public class AchievementAwardedListener implements Listener {

    private final GameManager gameManager;

    public AchievementAwardedListener(GameManager gm) {
        this.gameManager = gm;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onAchievementAwarded(final PlayerAdvancementDoneEvent event) {
        if (this.gameManager.getGameState() == GameState.GOING) {
            ManhuntPlayerV2 mPlayer = this.gameManager.getPlayerList().getManhuntPlayerByUUID(event.getPlayer().getUniqueId());
            if (mPlayer.getTeamType() != ManhuntTeamsTypesEnum.SPECTATOR) {
                sendAdvancementMessage(event, mPlayer);
                if (event.getAdvancement().getKey().getKey().equalsIgnoreCase("end/kill_dragon")) {
                    for (ManhuntPlayerV2 registerdPlayer : this.gameManager.getPlayerList().getPlayerList()) {
                        Bukkit.getScheduler().scheduleSyncDelayedTask(ManhuntMain.getPlugin(), () -> {
                            registerdPlayer.getMCPlayer().spigot().respawn();
                        }, 2);
                    }
                    Bukkit.getScheduler().scheduleSyncDelayedTask(ManhuntMain.getPlugin(), () -> {
                        this.gameManager.announceWinners(ManhuntTeamsTypesEnum.RUNNER);
                        this.gameManager.endGame();
                    }, 4);
                }
            }
        }
    }

    private void sendAdvancementMessage(final PlayerAdvancementDoneEvent event, ManhuntPlayerV2 mPlayer) {
        String namespace = event.getAdvancement().getKey().toString();
        namespace = "advancements." + namespace.substring(10);
        namespace = namespace.replace('/', '.');
        Bukkit.getLogger().log(Level.INFO, namespace);
        if (!namespace.contains("recipe") && !namespace.contains("root")) {
            TranslatableComponent message = new TranslatableComponent("chat.type.advancement.task");
            TranslatableComponent advancementTitleComponent = new TranslatableComponent(namespace + ".title");
            TranslatableComponent advancementDescComponent = new TranslatableComponent(namespace + ".description");
            advancementDescComponent.setColor(ChatColor.WHITE);

            advancementTitleComponent.setColor(ChatColor.GREEN);

            if (mPlayer.getTeamType() == ManhuntTeamsTypesEnum.RUNNER)
                message.addWith(ChatColor.GREEN + mPlayer.getName());
            else
                message.addWith(ChatColor.RED + mPlayer.getName());

            message.setColor(ChatColor.WHITE);
            BaseComponent[] components = { advancementDescComponent };
            advancementTitleComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text(components)));
            message.addWith(advancementTitleComponent);
            Bukkit.spigot().broadcast(message);
        }
    }
    
}
