package com.unshorisauvage.game;

import java.util.ArrayList;
import java.util.List;

import com.unshorisauvage.ManhuntMain;
import com.unshorisauvage.listeners.AchievementAwardedListener;
import com.unshorisauvage.listeners.BlockBreakListener;
import com.unshorisauvage.listeners.ChatListener;
import com.unshorisauvage.listeners.ItemIteractionListener;
import com.unshorisauvage.listeners.PlayerConnectionListener;
import com.unshorisauvage.listeners.PlayerDamageListener;
import com.unshorisauvage.listeners.PlayerDeathListener;
import com.unshorisauvage.listeners.PlayerDisconnectListener;
import com.unshorisauvage.listeners.PlayerMoveListener;
import com.unshorisauvage.listeners.PlayerRespawnListener;
import com.unshorisauvage.listeners.PlayerSneakListener;
import com.unshorisauvage.listeners.PlayerTeleportListener;
import com.unshorisauvage.players.ManhuntPlayerList;
import com.unshorisauvage.players.ManhuntPlayerV2;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;
import com.unshorisauvage.tasks.HuntersReleasedTask;
import com.unshorisauvage.util.LodestoneCompassUtil;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.GameRule;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.RecipeChoice.MaterialChoice;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class GameManager {
    
    public static GameManager GAME_MANAGER;
    
    private GameState gameState;

    private static List<Material> beds;
    private final FileConfiguration config;
    private UIThread uiThread;

    private final ManhuntPlayerList playerList;

    private ArrayList<World> worlds;

    private ItemStack reviveItem;

    public GameManager() {
        this.config = ManhuntMain.getPlugin().getConfig();
        this.worlds = new ArrayList<>();
        worlds.add(Bukkit.getWorld(this.config.getString("world_name")));
        worlds.add(Bukkit.getWorld(this.config.getString("world_name") + "_nether"));
        worlds.add(Bukkit.getWorld(this.config.getString("world_name") + "_the_end"));
        initBedsList();
        initGameRules();
        GAME_MANAGER = this;
        this.gameState = GameState.WAITING;

        this.playerList = new ManhuntPlayerList();

        this.uiThread = new UIThread();

        this.registerListeners();
        this.registerRecepies();

    }

    private void initGameRules() {
        for (World world : worlds) {
            world.setDifficulty(Difficulty.EASY);
            world.setTime(0L);
            world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
            world.setGameRule(GameRule.DISABLE_RAIDS, true);
            world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
            world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
            world.setGameRule(GameRule.KEEP_INVENTORY, false);
            world.setGameRule(GameRule.MAX_ENTITY_CRAMMING, 16);
        }
    }

    private void activateGameRules() {
        for (World world : worlds) {
            world.setDifficulty(Difficulty.EASY);
            world.setTime(0L);
            world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
            world.setGameRule(GameRule.DISABLE_RAIDS, true);
            world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, true);
            world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
        }
    }

    private void initBedsList() {
        beds = new ArrayList<>();
        beds.add(Material.WHITE_BED);
        beds.add(Material.ORANGE_BED);
        beds.add(Material.MAGENTA_BED);
        beds.add(Material.LIGHT_BLUE_BED);
        beds.add(Material.YELLOW_BED);
        beds.add(Material.LIME_BED);
        beds.add(Material.PINK_BED);
        beds.add(Material.GRAY_BED);
        beds.add(Material.LIGHT_GRAY_BED);
        beds.add(Material.CYAN_BED);
        beds.add(Material.PURPLE_BED);
        beds.add(Material.BLUE_BED);
        beds.add(Material.BROWN_BED);
        beds.add(Material.GREEN_BED);
        beds.add(Material.RED_BED);
        beds.add(Material.BLACK_BED);
    }

    public synchronized GameState getGameState() {
        return gameState;
    }

    public ItemStack getReviveItem() {
        return this.reviveItem;
    }

    public void setGameState(GameState gs) {
        this.gameState = gs;
    }

    public ManhuntPlayerList getPlayerList() {
        return this.playerList;
    }

    public void registerRecepies() {
        ItemStack item = new ItemStack(Material.SKULL_BANNER_PATTERN);
        item.addUnsafeEnchantment(Enchantment.MENDING, 1);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§aSortilège de Résurection");
        item.setItemMeta(meta);
        this.reviveItem = item;

        NamespacedKey key = new NamespacedKey(ManhuntMain.getPlugin(), "revive_item");

        ShapedRecipe recipe = new ShapedRecipe(key, item);
        recipe.shape("GDG","GBG","DGD");
        recipe.setIngredient('G', Material.GOLD_INGOT);
        recipe.setIngredient('D', Material.DIAMOND);
        recipe.setIngredient('B', new MaterialChoice(beds));

        Bukkit.addRecipe(recipe);
    }

    private void registerListeners() {
        List<Listener> listeners = new ArrayList<>();
        listeners.add(new AchievementAwardedListener(this));
        listeners.add(new BlockBreakListener(this));
        listeners.add(new ChatListener(this));
        listeners.add(new ItemIteractionListener(this));
        listeners.add(new PlayerConnectionListener(this));
        listeners.add(new PlayerDamageListener(this));
        listeners.add(new PlayerDeathListener(this));
        listeners.add(new PlayerDisconnectListener(this));
        listeners.add(new PlayerMoveListener(this));
        listeners.add(new PlayerRespawnListener(this));
        // listeners.add(new PlayerSneakListener(this));
        listeners.add(new PlayerTeleportListener(this));
        // listeners.add(new TestListener(this));
        for(Listener listener: listeners) {
            Bukkit.getServer().getPluginManager().registerEvents(listener, ManhuntMain.getPlugin());
        }
    }

    public void startGame() {
        activateGameRules();
        this.gameState = GameState.LOADING;
        int livesRemaing = this.config.getInt("extra_lives");
        if (livesRemaing <= 0) {
            livesRemaing = 3;
        }
        Bukkit.getServer().broadcastMessage("START: livesRemaing=" + livesRemaing);
        int headStartDuration = this.config.getInt("headstart_delay");
        if (headStartDuration < 0) {
            headStartDuration = 20;
        }

        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "advancement revoke @a everything");

        for (ManhuntPlayerV2 mPlayer : this.playerList.getPlayerList()) {
            Player p = mPlayer.getMCPlayer();
            switch (mPlayer.getTeamType()) {
                case HUNTER:
                    mPlayer.setRemaingLives(69);
                    p.setGameMode(GameMode.SURVIVAL);
                    p.setHealth(20D);
                    p.setFoodLevel(20);
                    p.setSaturation(15.0F);
                    p.getInventory().clear();
                    p.getInventory().addItem(LodestoneCompassUtil.getLodestoneCompass());
                    if (headStartDuration >= 1) {
                        p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * headStartDuration, 10));
                        p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20 * headStartDuration, 10));
                        p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 20 * headStartDuration, 10));
                        p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20 * headStartDuration, 10));
                        p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 20 * headStartDuration, 10));
                        p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 20 * headStartDuration, 10));
                    } else {
                        p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 50, 20));
                    }
                    
                    break;
                case RUNNER:
                    mPlayer.setRemaingLives(livesRemaing);
                    mPlayer.setAlive(true);
                    p.setGameMode(GameMode.SURVIVAL);
                    p.getInventory().clear();
                    p.setFoodLevel(20);
                    p.setHealth(20D);
                    p.setSaturation(15.0F);
                    p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 50, 20));
                    break;
                case SPECTATOR:
                    p.setGameMode(GameMode.SPECTATOR);
                    break;
                default:
                    break;
            }
            if (headStartDuration >= 1) {
                p.sendMessage("Les Chasseurs seront relachés dans " + headStartDuration + " senconde(s) !");
                Bukkit.getScheduler().scheduleSyncDelayedTask(ManhuntMain.getPlugin(), new HuntersReleasedTask(this), headStartDuration * 20);
                gameState = GameState.HEADSTART;
            } else {
                gameState = GameState.GOING;
            }
        }

        this.uiThread.startTask(this, ManhuntMain.getPlugin());
    }

    public void checkIfAliveRunners(){
        ArrayList<ManhuntPlayerV2> aliveRunnerList = this.playerList.getPlayerListByType(ManhuntTeamsTypesEnum.RUNNER);
        aliveRunnerList.removeIf(ManhuntPlayerV2::isDead);
        aliveRunnerList.removeIf(ManhuntPlayerV2::isNotOnline);
        int alivePlayers = aliveRunnerList.size();
        if (alivePlayers == 0) {
            this.gameState = GameState.WAITING;
            for (ManhuntPlayerV2 player: this.playerList.getPlayerList()) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(ManhuntMain.getPlugin(), () -> {
                    player.getMCPlayer().spigot().respawn();
                }, 2);
            }

            Bukkit.getScheduler().scheduleSyncDelayedTask(ManhuntMain.getPlugin(), () -> {
                announceWinners(ManhuntTeamsTypesEnum.HUNTER);
                endGame();
            }, 4);
        }
    }

    public void announceWinners(ManhuntTeamsTypesEnum winningTeamType) {
        String title = "TITLE";
        String subtitle = "gagnent la partie!";

        ArrayList<ManhuntPlayerV2> winnerList = this.playerList.getPlayerListByType(winningTeamType);
        if (winnerList.size() == 1) {
            ManhuntPlayerV2 winner = winnerList.get(0);
            title = winner.getName();
            subtitle = "gagne la partie!";
        } else {
            switch (winningTeamType) {
                case HUNTER:
                    title = "Les Chasseurs";
                    break;
                case RUNNER:
                    title = "Les Speedrunners";
                    break;
                default:
                    break;
            }
        }

        for (ManhuntPlayerV2 mPlayer : this.playerList.getPlayerList()) {
            Player p = mPlayer.getMCPlayer();
            p.sendTitle(title, subtitle, 20, 200, 20);
            if (mPlayer.getTeamType() == winningTeamType || mPlayer.getTeamType() == ManhuntTeamsTypesEnum.SPECTATOR) {
                p.playSound(p.getLocation(), Sound.UI_TOAST_CHALLENGE_COMPLETE, 1F, 0.69F);
            }
            else {
                p.playSound(p.getLocation(), Sound.ENTITY_ZOGLIN_DEATH, 1F, 0F);
            }
        }
    }

    public void endGame() {
        initGameRules();
        Bukkit.getScheduler().cancelTask(this.uiThread.getTaskId());
        this.gameState = GameState.WAITING;
    }
}
