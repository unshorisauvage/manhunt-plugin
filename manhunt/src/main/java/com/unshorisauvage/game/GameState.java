package com.unshorisauvage.game;

public enum GameState {
    WAITING,
    LOADING,
    HEADSTART,
    GOING,
}
