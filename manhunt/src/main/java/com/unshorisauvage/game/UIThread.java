package com.unshorisauvage.game;

import java.util.ArrayList;

import com.unshorisauvage.players.ManhuntPlayerV2;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;
import com.unshorisauvage.util.UIutil;

import org.bukkit.plugin.Plugin;

public class UIThread implements Runnable{

    private static GameManager gameManager;
    private int taskId;


    public void startTask(GameManager gm, Plugin pl) {
        this.taskId = pl.getServer().getScheduler().runTaskTimer(pl, this, 20L, 10L).getTaskId();
        gameManager = gm;
    }

    @Override
    public void run() {
        ArrayList<ManhuntPlayerV2> runners = gameManager.getPlayerList().getPlayerListByType(ManhuntTeamsTypesEnum.RUNNER);
        ArrayList<ManhuntPlayerV2> hunters = gameManager.getPlayerList().getPlayerListByType(ManhuntTeamsTypesEnum.HUNTER);

        for (ManhuntPlayerV2 runner : runners) {
            UIutil.updateActionBar(runner, runners);
        }

        for (ManhuntPlayerV2 hunter : hunters) {
            UIutil.updateActionBar(hunter, hunters);
        }
    }

    public int getTaskId() {
        return this.taskId;
    }
}
