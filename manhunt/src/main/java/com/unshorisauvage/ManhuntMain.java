package com.unshorisauvage;

import com.unshorisauvage.commands.ListCommand;
import com.unshorisauvage.commands.ManhuntStartCommand;
import com.unshorisauvage.commands.AllChatCommand;
import com.unshorisauvage.commands.DebugCommand;
import com.unshorisauvage.commands.HunterAddCommand;
import com.unshorisauvage.commands.RunnerAddCommand;
import com.unshorisauvage.commands.SpectateCommand;
import com.unshorisauvage.game.GameManager;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;


public class ManhuntMain extends JavaPlugin
{

    private GameManager gameManager;
    private static ManhuntMain plugin;
    FileConfiguration config = getConfig();

    @Override
    public void onEnable() {
        plugin = this;
        this.addDefaultConf();
        this.gameManager = new GameManager();
        getLogger().info("Enabled Manhunt.");
        
        this.getCommand("a").setExecutor(new AllChatCommand(gameManager));
        this.getCommand("runneradd").setExecutor(new RunnerAddCommand(gameManager));
        this.getCommand("hunteradd").setExecutor(new HunterAddCommand(gameManager));
        this.getCommand("spectate").setExecutor(new SpectateCommand(gameManager));
        this.getCommand("mlist").setExecutor(new ListCommand(gameManager));
        this.getCommand("mstart").setExecutor(new ManhuntStartCommand(gameManager));

        this.getCommand("mdebug").setExecutor(new DebugCommand(gameManager));
    }

    @Override
    public void onDisable() {
        getLogger().info("Disabled Manhunt.");
    }

    public static Plugin getPlugin() {
        return plugin;
    }

    private void addDefaultConf() {
        config.addDefault("world_name", "world");
        config.addDefault("headstart_delay", 20);
        config.addDefault("extra_lives", 3);
        config.options().copyDefaults(true);
        saveConfig();
    }

}
