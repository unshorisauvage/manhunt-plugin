package com.unshorisauvage.handlers;

import java.util.ArrayList;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.game.GameState;
import com.unshorisauvage.players.ManhuntPlayerV2;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;
import com.unshorisauvage.util.LodestoneCompassUtil;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class ItemInteractionHandler {

    private GameManager gm;

    public ItemInteractionHandler(GameManager gm) {
        this.gm = gm;
    }
    
    public void handleItemInteraction(final PlayerInteractEvent event) {
        if (event.hasItem()) {
            if (this.gm.getGameState() == GameState.GOING) {
                ManhuntPlayerV2 mPlayer = this.gm.getPlayerList().getManhuntPlayerByUUID(event.getPlayer().getUniqueId());
                ItemStack item = event.getItem();
                switch (item.getType()) {
                    case COMPASS:
                        this.handleCompass(item, mPlayer, event);
                    return;
                    case SKULL_BANNER_PATTERN:
                        this.handleSkullPattern(item, mPlayer, event);
                    return;
                    default:
                        return;
                }
            }
        }
    }

    private void handleCompass(ItemStack compass, ManhuntPlayerV2 mPlayer, PlayerInteractEvent event) {
        ItemStack item = event.getItem();
        // ManhuntPlayerV2 mPlayer = this.gm.getPlayerList().getManhuntPlayerByUUID(player.getUniqueId());
        if (event.hasItem()) {
            if (item.getType() == Material.COMPASS) {
                if (mPlayer.getTeamType() == ManhuntTeamsTypesEnum.HUNTER) {
                    ArrayList<ManhuntPlayerV2> runnerTeamList = this.gm.getPlayerList().getPlayerListByType(ManhuntTeamsTypesEnum.RUNNER);
                    // Bukkit.getLogger().log(Level.INFO, "size=" + runnerTeamList.size());
                    if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        boolean hasLoopedOnce = false;
                        int tracked = mPlayer.getTrackedPlayerIndex();
                        while(tracked < runnerTeamList.size() && !hasLoopedOnce) { // aled
                            tracked++;

                            if (tracked >= runnerTeamList.size()) {
                                tracked = 0;
                                hasLoopedOnce = true;
                            }

                            if (runnerTeamList.get(mPlayer.getTrackedPlayerIndex()).isAlive()) {
                                mPlayer.setTrackedPlayerIndex(tracked);
                                break;
                            } else {
                                tracked++;
                            }
                        }
                        ManhuntPlayerV2 trackedPlayer = runnerTeamList.get(mPlayer.getTrackedPlayerIndex());
                        mPlayer.getMCPlayer().sendMessage("Vous traquez " + ChatColor.GREEN + trackedPlayer.getName() + ChatColor.RESET + ".");
                        LodestoneCompassUtil.setLodestoneLocation(item, runnerTeamList.get(mPlayer.getTrackedPlayerIndex()).getMCPlayer().getLocation());
                        // Bukkit.getLogger().log(Level.INFO, event.getAction() + " | " + tracked + " AFTER");
                    } else if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                        // Bukkit.getLogger().log(Level.INFO, event.getAction() + " | " + tracked);
                        ManhuntPlayerV2 trackedPlayer = runnerTeamList.get(mPlayer.getTrackedPlayerIndex());
                        mPlayer.getMCPlayer().sendMessage("La position de " + ChatColor.GREEN + trackedPlayer.getName() + ChatColor.RESET + " à été actualisée.");
                        LodestoneCompassUtil.setLodestoneLocation(item, runnerTeamList.get(mPlayer.getTrackedPlayerIndex()).getMCPlayer().getLocation());
                    }
                }  
            }
        }
    }

    private void handleSkullPattern(ItemStack scroll, ManhuntPlayerV2 mPlayer, PlayerInteractEvent event) {
        if (scroll.getEnchantmentLevel(Enchantment.MENDING) >= 1 && mPlayer.getTeamType() == ManhuntTeamsTypesEnum.RUNNER) {
            ArrayList<ManhuntPlayerV2> deadPlayerList = this.gm.getPlayerList().getPlayerListByType(ManhuntTeamsTypesEnum.RUNNER);
            deadPlayerList.removeIf(ManhuntPlayerV2::cantBeRespawned);
            if (deadPlayerList.size() == 0) {
                event.setCancelled(true);
                mPlayer.getMCPlayer().sendMessage("Il n'y aucun équipier qui peut être ramené à la vie.");
                return;
            }

            for (ManhuntPlayerV2 deadPlayer : deadPlayerList) {
                if (mPlayer.equals(deadPlayer)) {
                    continue;
                }
                deadPlayer.reviveAtLocation(mPlayer.getMCPlayer().getLocation());
                deadPlayer.setAlive(true);
                Bukkit.broadcastMessage(ChatColor.GREEN + deadPlayer.getName() +
                                        ChatColor.WHITE + " a été ramené à la vie! Il lui reste " +
                                        ChatColor.GREEN + deadPlayer.getRemaingLives() + ChatColor.WHITE + "vie(s).");
            }
            mPlayer.getMCPlayer().getInventory().removeItem(scroll);
        }
    }
}
