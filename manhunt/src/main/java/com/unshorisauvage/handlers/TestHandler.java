package com.unshorisauvage.handlers;

import java.util.ArrayList;
import java.util.logging.Level;

import com.unshorisauvage.game.GameManager;
import com.unshorisauvage.players.ManhuntPlayerV2;
import com.unshorisauvage.players.ManhuntTeamsTypesEnum;
import com.unshorisauvage.util.LodestoneCompassUtil;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class TestHandler {

    private GameManager gm;
    private int tracked;

    public TestHandler(GameManager gm) {
        this.gm = gm;
        this.tracked = 0;
    }


    public void handleTest(final PlayerInteractEvent event) {
        Player player = event.getPlayer();
        ItemStack item = event.getItem();
        ManhuntPlayerV2 mPlayer = this.gm.getPlayerList().getManhuntPlayerByUUID(player.getUniqueId());
        if (event.hasItem()) {
            if (item.getType() == Material.COMPASS) {
                if (mPlayer.getTeamType() == ManhuntTeamsTypesEnum.HUNTER) {
                    ArrayList<ManhuntPlayerV2> runnerTeamList = this.gm.getPlayerList().getPlayerListByType(ManhuntTeamsTypesEnum.RUNNER);
                    Bukkit.getLogger().log(Level.INFO, "size=" + runnerTeamList.size());
                    if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
                        boolean hasLoopedOnce = false;

                        while(tracked < runnerTeamList.size() && !hasLoopedOnce) { // aled
                            tracked++;
                            if (runnerTeamList.get(tracked).isAlive()) {
                                break;
                            }

                            if (tracked >= runnerTeamList.size() - 1) {
                                tracked = 0;
                                hasLoopedOnce = true;
                            }
                        }

                        LodestoneCompassUtil.setLodestoneLocation(item, runnerTeamList.get(tracked).getMCPlayer().getLocation());
                        Bukkit.getLogger().log(Level.INFO, event.getAction() + " | " + tracked + " AFTER");
                    } else if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                        Bukkit.getLogger().log(Level.INFO, event.getAction() + " | " + tracked);
                        LodestoneCompassUtil.setLodestoneLocation(item, runnerTeamList.get(tracked).getMCPlayer().getLocation());
                    }
                } else {
                    Bukkit.getLogger().log(Level.INFO, "Not hunter");
                }   
            } else {
                Bukkit.getLogger().log(Level.INFO, "Not compass");
            }
        } else {
            Bukkit.getLogger().log(Level.INFO, "No item");
        }
    }
}
