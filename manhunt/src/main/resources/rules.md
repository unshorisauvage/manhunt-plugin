# Manhunt - Règles

## Principe

Le manhunt oppose deux équipes, ayant chacune des objectifs différents.
- Les Runners doivent tuer l'ender dragon (obtenir le succès "Free the End" en ayant au moins un membre encore en vie.)
- Les Chasseurs doivent les tuer.

## Equipes

### Runners
Des trucs à savoir sur les runners:
    - Lorsqu'un runner meurt, son stuff tombe au sol et il est placé en mode spectateur observant son équipier.
    - Les runners décédés peuvent être ranimés en fabriquant puis en utilisant un parchemin de résurrection.
    - Le craft du parchemin (à usage unique):   |G|D|G| G = Lingot d'or
                                                |-----| B = Lit
                                                |G|B|G| D = Diamant
                                                |-----| 
                                                |D|G|D| 
    - Les runners perdent si ils sont tous morts en simultané.

### Chasseurs
Des trucs à savoir sur les chasseurs:
    - Les chasseurs peuvent réapparaitre à l'infini, et une boussole leur est donnée à leur réapparition.
      (Aucune boussole n'est donnée en cas de perte, il faut en fabriquer une.)
    - La boussole indique qui est traqué et la direction du joueur, mais pas sa distance.
    - Vous pouvez changer le joueur traqué par la boussole en tapant avec.
    - Le clic-droit réactualise la position du joueur actuellement traqué.
    - Les chasseurs perdent s'ils tuent le dragon.

## Deroulement
Au début de la partie, les inventaires et les succès de tous les joueurs sont réinitialisés.
Les chasseurs sont aveuglés et immobilisés pendant 20 secondes. Une boussole leur est donnée. 
Les runners, eux, sont libres de leurs mouvements.

La partie se termine si le dragon est tué ou si tous les runners sont morts en simultanés.

## Gamerules
La difficulté est réglée sur facile.
Les raids contre les villages sont désactivés.
La météo est désactivée. (Il ne pleut jamais.)
Les succès sont visibles de tous les joueurs.

## Mods et ressoucespacks
Les mods comme Optifine ou une installation de Fabric sont OK.
La quasi totalité des ressourcespacks sont autorisés.

Evidemment pas de XRay, KillAura, d'autoclicker, et autre mods donnant un avantage significatif.